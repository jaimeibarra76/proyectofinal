USE [Electoral]
GO
/****** Object:  Table [dbo].[Administrador]    Script Date: 04/12/2019 02:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Administrador](
	[account] [varchar](20) NULL,
	[password] [varchar](20) NULL,
	[Status_admin] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Partido]    Script Date: 04/12/2019 02:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Partido](
	[Nombre] [varchar](20) NULL,
	[Candidato] [varchar](20) NULL,
	[Id_Paritdo] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Pk_Partido] PRIMARY KEY CLUSTERED 
(
	[Id_Paritdo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[VotosTotales]    Script Date: 04/12/2019 02:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[VotosTotales](
	[Partido] [varchar](50) NULL,
	[Votos] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Votantes]    Script Date: 04/12/2019 02:46:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Votantes](
	[Nombre] [nchar](20) NULL,
	[Apellidos] [varchar](50) NULL,
	[Colonia] [varchar](50) NULL,
	[Id_Votante] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [Pk_votante] PRIMARY KEY CLUSTERED 
(
	[Id_Votante] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[RegistrarVotantes]    Script Date: 04/12/2019 02:47:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[RegistrarVotantes] 
@Nombre varchar (20), @Apellidos varchar (50), @Colonia varchar (50)

as


if not exists (select Nombre from Votantes where Nombre=@Nombre)
insert into Votantes (Nombre,Apellidos,Colonia) values (@Nombre,@Apellidos,@Colonia)

else

update Votantes set  Nombre = @Nombre,Apellidos=@Apellidos,Colonia=@Colonia
GO
/****** Object:  StoredProcedure [dbo].[RegistrarPartido]    Script Date: 04/12/2019 02:47:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[RegistrarPartido] 
@Nombre varchar (20), @Candidato varchar (20)

as


if not exists (select Nombre from Partido where Nombre=@Nombre)
insert into Partido (Nombre,Candidato) values (@Nombre,@Candidato)

else

update Partido set Nombre = @Nombre,Candidato=@Candidato
GO
/****** Object:  StoredProcedure [dbo].[EliminarDatosVotos]    Script Date: 04/12/2019 02:47:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[EliminarDatosVotos]

@Partido varchar(50), @Votos int

as 
delete from VotosTotales where (Partido= @Partido)
delete from VotosTotales Where (Votos=@Votos)
GO
/****** Object:  StoredProcedure [dbo].[ ]    Script Date: 04/12/2019 02:47:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ ] 

@Partido varchar (20), @VotosTota int 

as 
if not exists (select Partido from VotosTotales where Partido=@Partido)
insert into VotosTotales (Partido,Votos) values (@Partido,@VotosTota)
else
update VotosTotales set  Votos=@VotosTota where Partido=@Partido
GO
