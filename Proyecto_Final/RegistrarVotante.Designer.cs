﻿namespace Proyecto_Final
{
    partial class RegistrarVotante
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRegistar = new System.Windows.Forms.Button();
            this.txtApellidoVotan = new System.Windows.Forms.TextBox();
            this.txtNombreVotan = new System.Windows.Forms.TextBox();
            this.txtColoniaVotan = new System.Windows.Forms.TextBox();
            this.lblnomvota = new System.Windows.Forms.Label();
            this.lblApevota = new System.Windows.Forms.Label();
            this.lblcolvota = new System.Windows.Forms.Label();
            this.lblRevota = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRegistar
            // 
            this.btnRegistar.Location = new System.Drawing.Point(89, 262);
            this.btnRegistar.Name = "btnRegistar";
            this.btnRegistar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistar.TabIndex = 7;
            this.btnRegistar.Text = "Registrar";
            this.btnRegistar.UseVisualStyleBackColor = true;
            this.btnRegistar.Click += new System.EventHandler(this.btnRegistar_Click);
            // 
            // txtApellidoVotan
            // 
            this.txtApellidoVotan.Location = new System.Drawing.Point(168, 133);
            this.txtApellidoVotan.Name = "txtApellidoVotan";
            this.txtApellidoVotan.Size = new System.Drawing.Size(173, 20);
            this.txtApellidoVotan.TabIndex = 6;
            // 
            // txtNombreVotan
            // 
            this.txtNombreVotan.Location = new System.Drawing.Point(168, 79);
            this.txtNombreVotan.Name = "txtNombreVotan";
            this.txtNombreVotan.Size = new System.Drawing.Size(173, 20);
            this.txtNombreVotan.TabIndex = 4;
            // 
            // txtColoniaVotan
            // 
            this.txtColoniaVotan.Location = new System.Drawing.Point(168, 184);
            this.txtColoniaVotan.Name = "txtColoniaVotan";
            this.txtColoniaVotan.Size = new System.Drawing.Size(173, 20);
            this.txtColoniaVotan.TabIndex = 8;
            // 
            // lblnomvota
            // 
            this.lblnomvota.AutoSize = true;
            this.lblnomvota.Location = new System.Drawing.Point(25, 79);
            this.lblnomvota.Name = "lblnomvota";
            this.lblnomvota.Size = new System.Drawing.Size(101, 13);
            this.lblnomvota.TabIndex = 9;
            this.lblnomvota.Text = "Nombre del Votante";
            // 
            // lblApevota
            // 
            this.lblApevota.AutoSize = true;
            this.lblApevota.Location = new System.Drawing.Point(21, 133);
            this.lblApevota.Name = "lblApevota";
            this.lblApevota.Size = new System.Drawing.Size(105, 13);
            this.lblApevota.TabIndex = 10;
            this.lblApevota.Text = "Apellidos del votante";
            // 
            // lblcolvota
            // 
            this.lblcolvota.AutoSize = true;
            this.lblcolvota.Location = new System.Drawing.Point(24, 191);
            this.lblcolvota.Name = "lblcolvota";
            this.lblcolvota.Size = new System.Drawing.Size(98, 13);
            this.lblcolvota.TabIndex = 11;
            this.lblcolvota.Text = "Colonia del votante";
            // 
            // lblRevota
            // 
            this.lblRevota.AutoSize = true;
            this.lblRevota.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRevota.Location = new System.Drawing.Point(92, 22);
            this.lblRevota.Name = "lblRevota";
            this.lblRevota.Size = new System.Drawing.Size(175, 24);
            this.lblRevota.TabIndex = 13;
            this.lblRevota.Text = "Registrar Votante";
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(234, 261);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 14;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // RegistrarVotante
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(377, 303);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.lblRevota);
            this.Controls.Add(this.lblcolvota);
            this.Controls.Add(this.lblApevota);
            this.Controls.Add(this.lblnomvota);
            this.Controls.Add(this.txtColoniaVotan);
            this.Controls.Add(this.btnRegistar);
            this.Controls.Add(this.txtApellidoVotan);
            this.Controls.Add(this.txtNombreVotan);
            this.Name = "RegistrarVotante";
            this.Text = "RegistrarVotante";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRegistar;
        private System.Windows.Forms.TextBox txtApellidoVotan;
        private System.Windows.Forms.TextBox txtNombreVotan;
        private System.Windows.Forms.TextBox txtColoniaVotan;
        private System.Windows.Forms.Label lblnomvota;
        private System.Windows.Forms.Label lblApevota;
        private System.Windows.Forms.Label lblcolvota;
        private System.Windows.Forms.Label lblRevota;
        private System.Windows.Forms.Button btnSalir;
    }
}