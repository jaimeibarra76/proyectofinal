﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Windows.Forms.DataVisualization.Charting;
using Milibreria;


namespace Proyecto_Final
{
    public partial class Grafica : Form
    {
        public Grafica()
        {
            InitializeComponent();
        }
        string CadenaConexion = "Data Source = localhost; Initial Catalog=Electoral; Integrated Security=Yes";
        SqlDataAdapter DA;
        DataTable DT;
        DataRow DR;
        string Consulta;

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Consulta = "SELECT * FROM VotosTotales";
            DA = new SqlDataAdapter(Consulta, CadenaConexion);
            DT = new DataTable();
            DA.Fill(DT);
            this.chart1.Palette = ChartColorPalette.SeaGreen;
            this.chart1.Titles.Add("Votos totales de las elecciones");
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i<DT.Rows.Count; i++)
                {
                    DR = DT.Rows[i];
                    Series series = this.chart1.Series.Add(DR.ItemArray[0].ToString());
                    series.Points.Add(Convert.ToDouble(DR.ItemArray[1]));
                }

              }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea salir?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                this.Close();
            }
        }
        


        private void btnBorrar_Click(object sender, EventArgs e)
        {
            
        }

       
    }
}
