﻿namespace Proyecto_Final
{
    partial class RegistrarPartido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNombrePar = new System.Windows.Forms.TextBox();
            this.txtCanidato = new System.Windows.Forms.TextBox();
            this.btnRegistar = new System.Windows.Forms.Button();
            this.lblNombrePar = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNombrePar
            // 
            this.txtNombrePar.Location = new System.Drawing.Point(125, 65);
            this.txtNombrePar.Name = "txtNombrePar";
            this.txtNombrePar.Size = new System.Drawing.Size(173, 20);
            this.txtNombrePar.TabIndex = 0;
            // 
            // txtCanidato
            // 
            this.txtCanidato.Location = new System.Drawing.Point(125, 132);
            this.txtCanidato.Name = "txtCanidato";
            this.txtCanidato.Size = new System.Drawing.Size(173, 20);
            this.txtCanidato.TabIndex = 2;
            // 
            // btnRegistar
            // 
            this.btnRegistar.Location = new System.Drawing.Point(70, 195);
            this.btnRegistar.Name = "btnRegistar";
            this.btnRegistar.Size = new System.Drawing.Size(75, 23);
            this.btnRegistar.TabIndex = 3;
            this.btnRegistar.Text = "Registrar";
            this.btnRegistar.UseVisualStyleBackColor = true;
            this.btnRegistar.Click += new System.EventHandler(this.btnRegistar_Click);
            // 
            // lblNombrePar
            // 
            this.lblNombrePar.AutoSize = true;
            this.lblNombrePar.Location = new System.Drawing.Point(13, 71);
            this.lblNombrePar.Name = "lblNombrePar";
            this.lblNombrePar.Size = new System.Drawing.Size(97, 13);
            this.lblNombrePar.TabIndex = 5;
            this.lblNombrePar.Text = "Nombre del Partido";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 135);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nombre del Candidato";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(63, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 24);
            this.label2.TabIndex = 7;
            this.label2.Text = "Registra tu Partido";
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(180, 195);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 8;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // RegistrarPartido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(322, 244);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lblNombrePar);
            this.Controls.Add(this.btnRegistar);
            this.Controls.Add(this.txtCanidato);
            this.Controls.Add(this.txtNombrePar);
            this.Name = "RegistrarPartido";
            this.Text = "RegistrarPartido";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombrePar;
        private System.Windows.Forms.TextBox txtCanidato;
        private System.Windows.Forms.Button btnRegistar;
        private System.Windows.Forms.Label lblNombrePar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSalir;
    }
}