﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Milibreria;

namespace Proyecto_Final
{
    public partial class ConsultarPartido : Consulta
    {
        public ConsultarPartido()
        {
            InitializeComponent();
        }

        private void ConsultarPartido_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = LlenarDataGV("Partido").Tables[0];
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNombre.Text.Trim()) == false)
            {
                try
                {
                    DataSet ds;

                    string cmd = "Select * FROM Partido WHERE Nombre LIKE ('%" + txtNombre.Text.Trim() + "%')";
                    ds = Utilidades.Ejecutar(cmd);

                    dataGridView1.DataSource = ds.Tables[0];
                }
                catch (Exception error)
                {
                    MessageBox.Show("A ocurrido un error..." + error.Message);
                }
            }
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {

        }
    }
}
