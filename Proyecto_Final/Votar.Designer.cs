﻿namespace Proyecto_Final
{
    partial class Votar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnNuevo = new System.Windows.Forms.Button();
            this.btnPartido = new System.Windows.Forms.Button();
            this.btnVotantes = new System.Windows.Forms.Button();
            this.btnVotar = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtCodigo = new MiLibreria1.ErrorTxtBos();
            this.txtVotante = new MiLibreria1.ErrorTxtBos();
            this.lblCliente = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.txtIDpar = new MiLibreria1.ErrorTxtBos();
            this.txtNombrePar = new MiLibreria1.ErrorTxtBos();
            this.txtCanidato = new MiLibreria1.ErrorTxtBos();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ColIDPar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColCandi = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSalir = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNuevo
            // 
            this.btnNuevo.Location = new System.Drawing.Point(395, 223);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(185, 23);
            this.btnNuevo.TabIndex = 34;
            this.btnNuevo.Text = "Nuevo voto";
            this.btnNuevo.UseVisualStyleBackColor = true;
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnPartido
            // 
            this.btnPartido.Location = new System.Drawing.Point(395, 194);
            this.btnPartido.Name = "btnPartido";
            this.btnPartido.Size = new System.Drawing.Size(185, 23);
            this.btnPartido.TabIndex = 33;
            this.btnPartido.Text = "Partidos";
            this.btnPartido.UseVisualStyleBackColor = true;
            this.btnPartido.Click += new System.EventHandler(this.btnPartido_Click);
            // 
            // btnVotantes
            // 
            this.btnVotantes.Location = new System.Drawing.Point(395, 165);
            this.btnVotantes.Name = "btnVotantes";
            this.btnVotantes.Size = new System.Drawing.Size(185, 23);
            this.btnVotantes.TabIndex = 32;
            this.btnVotantes.Text = "Votantes";
            this.btnVotantes.UseVisualStyleBackColor = true;
            this.btnVotantes.Click += new System.EventHandler(this.btnVotantes_Click);
            // 
            // btnVotar
            // 
            this.btnVotar.Location = new System.Drawing.Point(395, 136);
            this.btnVotar.Name = "btnVotar";
            this.btnVotar.Size = new System.Drawing.Size(185, 23);
            this.btnVotar.TabIndex = 30;
            this.btnVotar.Text = "Votar";
            this.btnVotar.UseVisualStyleBackColor = true;
            this.btnVotar.Click += new System.EventHandler(this.btnColocar_Click);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(239, 9);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(75, 23);
            this.btnBuscar.TabIndex = 46;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtCodigo
            // 
            this.txtCodigo.Location = new System.Drawing.Point(95, 12);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(138, 20);
            this.txtCodigo.SoloNumeros = false;
            this.txtCodigo.TabIndex = 45;
            this.txtCodigo.Validar = true;
            // 
            // txtVotante
            // 
            this.txtVotante.Location = new System.Drawing.Point(95, 51);
            this.txtVotante.Name = "txtVotante";
            this.txtVotante.Size = new System.Drawing.Size(195, 20);
            this.txtVotante.SoloNumeros = false;
            this.txtVotante.TabIndex = 44;
            this.txtVotante.Validar = true;
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCliente.Location = new System.Drawing.Point(15, 52);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(59, 15);
            this.lblCliente.TabIndex = 43;
            this.lblCliente.Text = "Votante:";
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(15, 10);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(56, 15);
            this.lblCodigo.TabIndex = 42;
            this.lblCodigo.Text = "Codigo:";
            // 
            // txtIDpar
            // 
            this.txtIDpar.Location = new System.Drawing.Point(12, 98);
            this.txtIDpar.Name = "txtIDpar";
            this.txtIDpar.Size = new System.Drawing.Size(100, 20);
            this.txtIDpar.SoloNumeros = false;
            this.txtIDpar.TabIndex = 49;
            this.txtIDpar.Validar = true;
            // 
            // txtNombrePar
            // 
            this.txtNombrePar.Location = new System.Drawing.Point(118, 98);
            this.txtNombrePar.Name = "txtNombrePar";
            this.txtNombrePar.Size = new System.Drawing.Size(100, 20);
            this.txtNombrePar.SoloNumeros = false;
            this.txtNombrePar.TabIndex = 47;
            this.txtNombrePar.Validar = true;
            // 
            // txtCanidato
            // 
            this.txtCanidato.Location = new System.Drawing.Point(224, 98);
            this.txtCanidato.Name = "txtCanidato";
            this.txtCanidato.Size = new System.Drawing.Size(100, 20);
            this.txtCanidato.SoloNumeros = false;
            this.txtCanidato.TabIndex = 50;
            this.txtCanidato.Validar = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColIDPar,
            this.ColCandi,
            this.ColPrecio});
            this.dataGridView1.Location = new System.Drawing.Point(9, 136);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView1.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(345, 235);
            this.dataGridView1.TabIndex = 51;
            // 
            // ColIDPar
            // 
            this.ColIDPar.HeaderText = "Codigo Partido";
            this.ColIDPar.Name = "ColIDPar";
            this.ColIDPar.ReadOnly = true;
            // 
            // ColCandi
            // 
            this.ColCandi.HeaderText = "Candidato";
            this.ColCandi.Name = "ColCandi";
            this.ColCandi.ReadOnly = true;
            this.ColCandi.Width = 130;
            // 
            // ColPrecio
            // 
            this.ColPrecio.HeaderText = "Votos";
            this.ColPrecio.Name = "ColPrecio";
            this.ColPrecio.ReadOnly = true;
            this.ColPrecio.Width = 110;
            // 
            // btnSalir
            // 
            this.btnSalir.BackColor = System.Drawing.SystemColors.Control;
            this.btnSalir.Location = new System.Drawing.Point(506, 348);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 52;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = false;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(395, 253);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(185, 23);
            this.btnEliminar.TabIndex = 53;
            this.btnEliminar.Text = "Eliminar Votos Totales";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click_1);
            // 
            // Votar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(593, 383);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txtCanidato);
            this.Controls.Add(this.txtIDpar);
            this.Controls.Add(this.txtNombrePar);
            this.Controls.Add(this.btnBuscar);
            this.Controls.Add(this.txtCodigo);
            this.Controls.Add(this.txtVotante);
            this.Controls.Add(this.lblCliente);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.btnPartido);
            this.Controls.Add(this.btnVotantes);
            this.Controls.Add(this.btnVotar);
            this.Name = "Votar";
            this.Text = "Votar";
            this.Load += new System.EventHandler(this.Votar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnNuevo;
        private System.Windows.Forms.Button btnPartido;
        private System.Windows.Forms.Button btnVotantes;
        private System.Windows.Forms.Button btnVotar;
        private System.Windows.Forms.Button btnBuscar;
        private MiLibreria1.ErrorTxtBos txtCodigo;
        private MiLibreria1.ErrorTxtBos txtVotante;
        private System.Windows.Forms.Label lblCliente;
        private System.Windows.Forms.Label lblCodigo;
        private MiLibreria1.ErrorTxtBos txtIDpar;
        private MiLibreria1.ErrorTxtBos txtNombrePar;
        private MiLibreria1.ErrorTxtBos txtCanidato;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColIDPar;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColCandi;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColPrecio;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.Button btnEliminar;
    }
}