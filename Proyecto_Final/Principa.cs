﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Proyecto_Final
{
    public partial class Principa : Form
    {
        private int childFormNumber = 0;

        public Principa()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Ventana " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Archivos de texto (*.txt)|*.txt|Todos los archivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void partidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistrarPartido RePAR = new RegistrarPartido();
            RePAR.MdiParent = this;
            RePAR.Show();
        }

        private void votanteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RegistrarVotante ReVOT = new RegistrarVotante();
            ReVOT.MdiParent = this;
            ReVOT.Show();
        }

        private void consultarVotantesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarVotante ConVota = new ConsultarVotante();
            ConVota.MdiParent = this;
            ConVota.Show();
        }

        private void consultarPartidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarPartido ConPar = new ConsultarPartido();
            ConPar.MdiParent = this;
            ConPar.Show();
        }

        private void graficaDeResultadosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Grafica Gra = new Grafica();
            Gra.MdiParent = this;
            Gra.Show();
        }

        private void votarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Votar Vota = new Votar();
            Vota.MdiParent = this;
            Vota.Show();
        }

        private void Principa_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
