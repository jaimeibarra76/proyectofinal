﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Milibreria;

namespace Proyecto_Final
{
    public partial class Votar : Form
    {
        public Votar()
        {
            InitializeComponent();
        }
        public static int cont_fila = 0;
        public static double votosTota = 0;
        private void btnColocar_Click(object sender, EventArgs e)
        {
            bool existe = false;
            int num_fila = 0;
            if (cont_fila == 0)
            {
                dataGridView1.Rows.Add(txtIDpar.Text, txtCanidato.Text, 1);
                cont_fila++;


            }
            else
            {
                foreach (DataGridViewRow Fila in dataGridView1.Rows)
                {
                    if (Fila.Cells[0].Value.ToString() == txtIDpar.Text)
                    {
                        existe = true;
                        num_fila = Fila.Index;
                    }
                }

                if (existe == true)
                {
                    dataGridView1.Rows[num_fila].Cells[2].Value = 1 + Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[2].Value);
                    votosTota = Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[2].Value);
                }
                else
                {
                    dataGridView1.Rows.Add(txtIDpar.Text, txtCanidato.Text, 1);
                    votosTota = Convert.ToDouble(dataGridView1.Rows[num_fila].Cells[2].Value);
                }
            }

            Guardar();
        }

        public Boolean Vota()
        {
            try
            {
                string cmd = string.Format("EXEC RegistrarPartido '{0}','{1}','{2}'", txtIDpar.Text.Trim(), txtNombrePar.Text.Trim(), txtCanidato.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("Se ha guardado correctamente..");
                return true;
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
                return false;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCodigo.Text.Trim()) == false)
            {
                try
                {
                    string cmd = string.Format("Select Nombre FROM Votantes where Id_Votante ='{0}'", txtCodigo.Text.Trim());


                    DataSet ds = Utilidades.Ejecutar(cmd);

                    txtVotante.Text = ds.Tables[0].Rows[0]["Nombre"].ToString().Trim();

                    txtIDpar.Focus();
                }
                catch (Exception error)
                {
                    MessageBox.Show("Ha ocurrido un error..." + error.Message);
                }
            }
        }

        private void btnVotantes_Click(object sender, EventArgs e)
        {
            ConsultarVotante ConVota = new ConsultarVotante();
            ConVota.ShowDialog();

            if (ConVota.DialogResult == DialogResult.OK)
            {
                txtCodigo.Text = ConVota.dataGridView1.Rows[ConVota.dataGridView1.CurrentRow.Index].Cells[3].Value.ToString();
                txtVotante.Text = ConVota.dataGridView1.Rows[ConVota.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();

                txtIDpar.Focus();
            }
        }

        private void btnPartido_Click(object sender, EventArgs e)
        {
            ConsultarPartido ConPar = new ConsultarPartido();
            ConPar.ShowDialog();

            if (ConPar.DialogResult == DialogResult.OK)
            {
                txtIDpar.Text = ConPar.dataGridView1.Rows[ConPar.dataGridView1.CurrentRow.Index].Cells[2].Value.ToString();
                txtNombrePar.Text = ConPar.dataGridView1.Rows[ConPar.dataGridView1.CurrentRow.Index].Cells[0].Value.ToString();
                txtCanidato.Text = ConPar.dataGridView1.Rows[ConPar.dataGridView1.CurrentRow.Index].Cells[1].Value.ToString();
                btnVotar.Focus();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            txtIDpar.Text = "";
            txtVotante.Text = "";
            txtCodigo.Text = "";
            txtNombrePar.Text = "";
            txtCanidato.Text = "";
            cont_fila = 0;
            txtCodigo.Focus();
            dataGridView1.Rows.Clear();
        }

        private void btnGuardarVotos_Click(object sender, EventArgs e)
        {
            
        }

        public Boolean Guardar()
        {
            try
            {
                string cmd = string.Format("EXEC VotosTotale '{0}','{1}'", txtNombrePar.Text.Trim(), votosTota);
                Utilidades.Ejecutar(cmd);
                return true;
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error: " + error.Message);
                return false;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea salir?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
        }
        public DataSet LlenarDataGV(string tabla)
        {
            DataSet DS;

            string cmd = string.Format("SELECT * FROM " + tabla);
            DS = Utilidades.Ejecutar(cmd);

            return DS;
        }

        private void Votar_Load(object sender, EventArgs e)
        {

            
        }

        public void Eliminar()
        {
            try
            {
                string cmd = string.Format("Delete  from VotosTotales");
                Utilidades.Ejecutar(cmd);
            }
            catch (Exception error)
            {
                MessageBox.Show("Ha ocurrido un error..." + error.Message);
            }
        }

        private void btnEliminar_Click_1(object sender, EventArgs e)
        {
            Eliminar();
        }
    }
}
