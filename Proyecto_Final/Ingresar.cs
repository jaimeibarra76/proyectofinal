﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Milibreria;

namespace Proyecto_Final
{
    public partial class Ingresar : Form
    {
        public Ingresar()
        {
            Thread t = new Thread(new ThreadStart(inicioStart));
            InitializeComponent();
            t.Start();
            Thread.Sleep(7000);
            t.Abort();
        }

        public void inicioStart()
        {
            Application.Run(new Splash());
        }
        private void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                string CMD = string.Format("Select * FROM Administrador WHERE account='{0}' AND password ='{1}'", txtNomacc.Text.Trim(), txtPass.Text.Trim());

                DataSet ds = Utilidades.Ejecutar(CMD);

                string cuenta = ds.Tables[0].Rows[0]["account"].ToString().Trim();
                string contra = ds.Tables[0].Rows[0]["password"].ToString().Trim();

                if (cuenta == txtNomacc.Text.Trim() && contra == txtPass.Text.Trim())
                {
                  
                    Principa Prin = new Principa();
                    this.Hide();
                    Prin.Show();
                }

            }
            catch (Exception error)
            {
                MessageBox.Show("Error:" + error.Message);
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea salir?", "Aviso", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
            {
                this.Close();
            }
        }
    }
}
